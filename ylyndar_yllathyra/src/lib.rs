use std::io::Result;
use std::os::unix::net;

pub struct Go<F> {
    pub listener: net::UnixListener,
    pub f: F,
}

impl<F> Go<F> {
    pub fn new(
        tmp_path: impl AsRef<std::path::Path>,
        path: impl AsRef<std::path::Path>,
        f: F,
    ) -> Result<Self> {
        let _ = std::fs::remove_file(&tmp_path);
        let listener = net::UnixListener::bind(&tmp_path)?;
        std::fs::rename(tmp_path, path)?;

        Ok(Go { listener, f })
    }
}

impl<F, R> Iterator for Go<F>
where
    F: FnMut((net::UnixStream, net::SocketAddr)) -> R,
{
    type Item = Result<R>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.listener.accept().map(&mut self.f))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (usize::MAX, None)
    }
}
