use std::io;
use std::process::{self as proc, ChildStdin, ChildStdout, Stdio};

#[derive(Debug)]
pub struct Go<O: io::Read> {
    stdin: ChildStdin,
    stdout: O,
}

impl<O: io::Read> io::Write for Go<O> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.stdin.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.stdin.flush()
    }
}

impl<O: io::Read> io::Write for &Go<O> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        (&self.stdin).write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        (&self.stdin).flush()
    }
}

impl<O: io::Read> io::Read for Go<O> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.stdout.read(buf)
    }
}

pub fn go(mut shell: std::process::Command) -> io::Result<(Go<ChildStdout>, proc::ChildStderr)> {
    let mut child = shell
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    Ok((
        Go {
            stdin: child.stdin.take().unwrap(),
            stdout: child.stdout.take().unwrap(),
        },
        child.stderr.take().unwrap(),
    ))
}

#[cfg(feature = "rustix")]
pub struct Rustix(rustix::io::OwnedFd);

#[cfg(feature = "rustix")]
impl io::Read for Rustix {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        rustix::io::read(&self.0, buf).map_err(Into::into)
    }
}

#[cfg(feature = "rustix")]
impl io::Read for &Rustix {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        rustix::io::read(&self.0, buf).map_err(Into::into)
    }
}

#[cfg(feature = "rustix")]
pub fn go_with_stderr(mut shell: std::process::Command) -> io::Result<Go<Rustix>> {
    use command_fds::{CommandFdExt, FdMapping};
    use rustix::fd::AsRawFd;

    let (stdout_fd, stdout) = rustix::io::pipe()?;

    let stderr = rustix::io::dup(&stdout)?;

    let mut child = shell
        .stdin(Stdio::piped())
        .fd_mappings(vec![
            FdMapping {
                parent_fd: stdout.as_raw_fd(),
                child_fd: linux_raw_sys::general::STDOUT_FILENO as _,
            },
            FdMapping {
                parent_fd: stderr.as_raw_fd(),
                child_fd: linux_raw_sys::general::STDERR_FILENO as _,
            },
        ])
        .unwrap()
        .spawn()?;

    Ok(Go {
        stdin: child.stdin.take().unwrap(),
        stdout: Rustix(stdout_fd),
    })
}
